AWS Eye
=========

Get records from an AWS Route53 hosted zone

For information on how to set up your AWS access key and secret access key,
visit the following link

`Boto3 configuration <http://boto3.readthedocs.io/en/latest/guide/quickstart.html#configuration>`_


Usage
=====

.. code::

    # Syntax 
    python awseye.py domain-name [-f csv|json]
    
    # Example: Export all records to a csv file
    python awseye.py personalblog.me -f csv > personalblog_records.csv

    # Example: Export all records which value equals to 54.197.229.75
    python awseye.py mystore.com -f csv -s 54.197.229.75 > mystore_records.csv

