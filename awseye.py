#!/usr/bin/env python
# coding=utf-8
import boto3
import json
import jmespath
import argparse
import csv
import six
import io


class AwsEye(object):

    def __init__(self):
        self._client = boto3.client('route53')

    def get_hosted_zone_info(self, name):
        hosted_zones = self._client.list_hosted_zones()
        hosted_zone = jmespath.search(
            'HostedZones[?Name==`%s`]' % name,
            hosted_zones
        )
        return hosted_zone

    def get_hosted_zone_record_sets(self, hosted_zone_id, max_items=100):
        next_record_name = None
        records = []
        params = {
            'HostedZoneId': hosted_zone_id,
            'MaxItems': str(max_items)
        }

        while True:
            if next_record_name:
                params.update({'StartRecordName': next_record_name})

            page = self._client.list_resource_record_sets(**params)
            records += page['ResourceRecordSets']
            next_record_name = page.get('NextRecordName', None)

            if not page.get('IsTruncated', False):
                break

        return {'ResourceRecordSets': records}

    def get_domain_record_sets(self, domain_name):
        info = self.get_hosted_zone_info(domain_name + '.')
        if len(info) == 0:
            raise Exception(
                "Domain name '%s' has not been found" % domain_name
            )

        records = self.get_hosted_zone_record_sets(info[0]['Id'])
        return records

    def get_filtered_domain_record_sets(self, domain_name, value):
        records = self.get_domain_record_sets(domain_name)

        exp = 'ResourceRecordSets[?ResourceRecords[?Value==`%s`]]' % value
        return {'ResourceRecordSets': jmespath.search(exp, records)}


class OutputFormatter(object):
    def factory(format):
        if format == 'json':
            formatter = JsonFormatter()
        elif format == 'csv':
            formatter = CsvFormatter()
        else:
            formatter = CsvFormatter()
        return formatter

    factory = staticmethod(factory)


class CsvFormatter(object):

    def format(self, records):
        if six.PY3:
            f = io.StringIO()
        else:
            f = io.BytesIO()

        writer = csv.DictWriter(
            f, fieldnames=(u"name", u"type", u"ttl", u"value")
        )
        writer.writeheader()

        for rec in records['ResourceRecordSets']:
            row = {
                "name": rec['Name'],
                "type": rec['Type'],
                "ttl": rec['TTL']
            }
            for pair in rec['ResourceRecords']:
                row["value"] = pair['Value']
                writer.writerow(row)

        return f.getvalue()


class JsonFormatter(object):

    def format(self, records):
        return json.dumps(resp, indent=3)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description="Gets information from AWS Route53 hosted zones"
    )
    parser.add_argument('domain', type=str, action='store', default=None)
    parser.add_argument(
        '-s', '--search', type=str,
        help='Filter output only to the records matching the given value',
        action='store', default='')
    parser.add_argument(
        '-f', '--format', type=str,
        help='Specifies the output format: json or csv',
        action='store', default='json'
    )
    args = parser.parse_args()

    eye = AwsEye()
    resp = []
    if args.search == '':
        resp = eye.get_domain_record_sets(args.domain)
    else:
        resp = eye.get_filtered_domain_record_sets(
            args.domain, value=args.search
        )

    formatter = OutputFormatter.factory(args.format)
    resp = formatter.format(resp)
    print(resp)
